## cactus-user 9 PPR1.180610.011 V12.0.2.0.PCBCNXM release-keys
- Manufacturer: xiaomi
- Platform: 
- Codename: cactus
- Brand: Xiaomi
- Flavor: cipher_cactus-userdebug
- Release Version: 12
- Id: SQ1D.220205.003
- Incremental: eng.root.20220222.144155
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: xiaomi/cactus/cactus:9/PPR1.180610.011/V12.0.2.0.PCBCNXM:user/release-keys
- OTA version: 
- Branch: cactus-user-9-PPR1.180610.011-V12.0.2.0.PCBCNXM-release-keys
- Repo: xiaomi_cactus_dump_7151


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
